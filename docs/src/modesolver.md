# modesolver.jl

## Description

This file contains the code for creating the eigenmatrix for the finite
difference method problem, solving it, and the returning the results
in a useful manner. It should handle both 1D and 2D index structures on
both uniformly and non-uniformly sampled grids via the `waveguidemodes`
function. The `waveguidemodes` function formulates the eigenmatrix
itself, then hands it off to `modesolver`. `modesolver` takes the
eigensolutions from `eigensolve` and processes them as appropriate
(ensuring mode solutions do not have a real effective index component
below that of the waveguide values, and that the solutions are ordered
in decreasing real effective index). `eigensolve` itself wraps around
the eigensolving packages/functions, namely `LinearAlgebra.eigen` and
`Arpack.eigs`. It attempts to choose the best function based on what
matrix type the eigenmatrix is.

TODO: Add benchmark/evaluation of the eigensolving functions to motivate their choice

## Functions

```@autodocs
Modules = [FDMModes]
Pages   = ["modesolver.jl"]
```
