# References

The very basic finite difference method mode solving algorithm is described
in Appendix 17 of 
["Diode Lasers and Photonic Integrated Circuits"](https://coldren.ece.ucsb.edu/book).
The chapter provides a description of a uniform grid scalar mode solver,
and provides reference results for a 1D problem and implementation.
The non-uniform implementations are created by deriving the non-uniform
grid Laplacian operator matrix and making the appropriate modification
to the basic algorithm (a good reference for this has not yet been
made/found).

An alternative and more advanced set of mode solvers can be found in
the
[WGMODES](https://photonics.umd.edu/software/wgmodes/)
MATLAB program
(
[GitLab mirror](https://gitlab.com/pawelstrzebonski/WGMODES)
and Julia port
[WGMODES.jl](https://gitlab.com/pawelstrzebonski/WGMODES.jl)
).
The `svmodes` function provided by WGMODES is most similar to `FDMModes`
(2D scalar mode solver, but with additional features like anisotropic media
and advanced boundary conditions). This is a good source to cross-validate
the results of this package.
