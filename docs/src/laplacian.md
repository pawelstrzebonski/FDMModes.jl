# laplacian.jl

## Description

This file implements the various forms of the finite difference matrix
operators, namely the
Laplacian matrix operator, in its 1D and 2D, uniform and non-uniform
grid versions.

The uniform grid 1D Laplacian matrix operator is a
symmetric tridiagonal matrix with $-\frac{2}{\Delta x^2}$ on the diagonal
and $\frac{1}{\Delta x^2}$ on the +1 and -1 diagonals. The 2D Laplacian
matrix operator can be derived from the 1D Laplacian matrix operators
as the Kronecker product of the 1D matrix operators for each of the two
axes, so that $\nabla^2_{xy}=\nabla^2_x\otimes\nabla^2_y$.

For a 1D Laplacian on a non-uniform grid, we derive a different set
of finite-difference coefficients for the matrix so that we get get a
tridiagonal matrix that is not symmetric. We can similarly use the
Kronecker product to obtain a 2D non-uniform Laplacian matrix operator
from the 1D non-uniform Laplacian matrix operators.

## Functions

```@autodocs
Modules = [FDMModes]
Pages   = ["laplacian.jl"]
```
